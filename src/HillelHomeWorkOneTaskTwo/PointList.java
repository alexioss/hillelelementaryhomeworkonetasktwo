package HillelHomeWorkOneTaskTwo;

import java.util.ArrayList;

public class PointList {

    private ArrayList<Point> points = new ArrayList<>();

    public void addPoint(Point point){

        points.add(point);
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

}
