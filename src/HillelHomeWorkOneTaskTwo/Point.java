package HillelHomeWorkOneTaskTwo;

public class Point {

    private int coordinatesX;
    private int coordinatesY;

    public Point(int coordinatesX, int coordinatesY) {
        this.coordinatesX = coordinatesX;
        this.coordinatesY = coordinatesY;
    }

    public double distanceBetweenPoint(Point point) {

        return Math.sqrt(Math.pow(coordinatesX - point.coordinatesX,2) + Math.pow(coordinatesY - point.coordinatesY,2));
    }

    @Override
    public String toString() {
        return "Point{" +
                "coordinatesX=" + coordinatesX +
                ", coordinatesY=" + coordinatesY +
                '}';
    }
}
