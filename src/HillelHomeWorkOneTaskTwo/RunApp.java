package HillelHomeWorkOneTaskTwo;

import java.util.Scanner;

public class RunApp {

    private Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        PointList pointList = new PointList();
        RunApp runApp = new RunApp();

        pointList.addPoint(runApp.getInputCoordinatePoint());
        boolean isNeedToStop = true;
        while (isNeedToStop) {
            int userChoice = runApp.getInputUserChoice();
            switch (userChoice) {
                case 1:
                    runApp.scanner.nextLine();
                    pointList.addPoint(runApp.getInputCoordinatePoint());
                    continue;
                case 2:
                    isNeedToStop = false;
                    break;
            }
        }

        System.out.println("Необходимо ввести ккординаты центра окружности: ");
        runApp.scanner.nextLine();
        Point centerCircle = runApp.getInputCoordinatePoint();

        System.out.println("Необходимо ввести радиус окружности: ");
        int circleRadius = runApp.getRadiusUserChoice();
        Circle circle = new Circle(centerCircle, circleRadius);

        runApp.getPointsBelongsCircle(circle, pointList);


    }

    public Point getInputCoordinatePoint() {

        System.out.println("Введите координаты точки!");
        System.out.print("Координата Х: ");

        while (!scanner.hasNextInt()) {
            String str = scanner.nextLine().trim();
            System.out.printf("\"%s\" - не число!\n", str);
            System.out.print("Координата Х: ");
        }
        int coordinateX = scanner.nextInt();

        scanner.nextLine();
        System.out.print("Координата Y: ");
        while (!scanner.hasNextInt()) {
            String str = scanner.nextLine().trim();
            System.out.printf("\"%s\" - не число!\n", str);
            System.out.print("Координата Y: ");
        }
        int coordinateY = scanner.nextInt();

        return new Point(coordinateX, coordinateY);

    }

    public int getInputUserChoice() {

        System.out.println("Желаете добавить еще (1 - да; 2 - нет)!");
        System.out.print("Ваш выбор: ");
        scanner.nextLine();

        boolean isNeedToStop = true;
        while (isNeedToStop) {
            while (!scanner.hasNextInt()) {
                String str = scanner.nextLine().trim();
                System.out.printf("\"%s\" - не число!\n", str);
                System.out.print("Ваш выбор: ");
            }

            int userChoice = scanner.nextInt();

            switch (userChoice) {
                case 1:
                case 2:
                    return userChoice;
                default:
                    System.out.println("Пожалуйста сделайте выбор из предложеного!");
                    System.out.print("Ваш выбор: ");
            }
        }
        return 0;
    }

    public int getRadiusUserChoice() {

        scanner.nextLine();
        boolean isNeedToStop = true;
        while (isNeedToStop) {

            while (!scanner.hasNextInt()) {
                String str = scanner.nextLine().trim();
                System.out.printf("\"%s\" - не число!\n", str);
                System.out.print("Ваш выбор: ");
            }

            int radiusUserChoice = scanner.nextInt();
            if (radiusUserChoice < 0) {
                System.out.println("Радиус не может быть меньше нуля!");
                System.out.print("Ваш выбор: ");
                continue;
            } else
                return radiusUserChoice;
        }
        return 0;
    }

    public void getPointsBelongsCircle(Circle circle, PointList pointList) {

        System.out.println("Точки принадлежащие окружности:");
        int countPoint = 0;
        for (int i = 0; i < pointList.getPoints().size(); i++) {
            if (circle.checkPointBelongsCircle(pointList.getPoints().get(i))) {
                System.out.println(pointList.getPoints().get(i).toString());
                countPoint++;
            }
        }
        if (countPoint == 0) {
            System.out.println("Таких точек нет!");
        }
    }

}
