package HillelHomeWorkOneTaskTwo;

public class Circle {

    private Point circleCenter;
    private int circleRadius;

    public Circle(Point circleCenter, int circleRadius) {

        this.circleCenter = circleCenter;
        this.circleRadius = circleRadius;
    }

    public boolean checkPointBelongsCircle(Point point){

        if (circleCenter.distanceBetweenPoint(point) < circleRadius){
            return true;
        }
        return false;
    }
}
